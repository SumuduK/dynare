dnl Process this file with autoconf to produce a configure script.

dnl Copyright (C) 2009-2018 Dynare Team
dnl
dnl This file is part of Dynare.
dnl
dnl Dynare is free software: you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation, either version 3 of the License, or
dnl (at your option) any later version.
dnl
dnl Dynare is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

AC_PREREQ([2.62])
AC_INIT([dynare], [4.6-unstable])
AC_CONFIG_SRCDIR([preprocessor/src/DynareMain.cc])
AM_INIT_AUTOMAKE([1.11 -Wall -Wno-portability foreign no-dist-gzip dist-xz tar-pax])

AC_CONFIG_SUBDIRS([preprocessor])

AC_PROG_CC
AC_PROG_CXX
AX_CXX_COMPILE_STDCXX_14

AC_CANONICAL_HOST
case ${host_os} in
  *mingw32*)
    # On Windows, we want Dynare++ to be statically linked
    AM_LDFLAGS="-static"
    AC_SUBST([AM_LDFLAGS])
    ;;
  *cygwin*)
    AC_MSG_WARN([You are compiling for the Cygwin target. This means that the preprocessor will])
    AC_MSG_WARN([not run from MATLAB unless you add the Cygwin DLL to the path.])
    AC_MSG_WARN([This is probably not what you want. Consider using a MinGW cross-compiler.])
    if test -z "$F77"; then
      # On Cygwin 1.7, g77 comes has version 3, and is not compatible with default gcc/g++ which has version 4
      # And by default, the AC_PROG_F77 will pick up g77 if it is present (even if gfortran is also here)
      F77=gfortran
    fi
    ;;
esac

# Use C++ for testing headers
AC_LANG([C++])

AM_CXXFLAGS="-Wall -Wno-parentheses"
AC_SUBST([AM_CXXFLAGS])

# If default 'ar' is not available, try to find one with a host prefix (see ticket #145)
AC_CHECK_PROGS([AR], [ar ${host_alias}-ar])

AC_PROG_RANLIB
AM_PROG_AR

AX_PROG_LN_S

AC_PROG_MKDIR_P

AX_CXX11_THREAD

# Check for libmatio, needed by Dynare++
AX_MATIO
AM_CONDITIONAL([HAVE_MATIO], [test "$has_matio" = yes])

AC_CHECK_PROG([PDFLATEX], [pdflatex], [pdflatex])
AM_CONDITIONAL([HAVE_PDFLATEX], [test -n "$PDFLATEX"])

AC_CHECK_PROG([BIBTEX], [bibtex], [bibtex])
AM_CONDITIONAL([HAVE_BIBTEX], [test -n "$BIBTEX"])

AC_CHECK_PROG([SPHINXBUILD], [sphinx-build], [sphinx-build])
AM_CONDITIONAL([HAVE_SPHINXBUILD], [test -n "$SPHINXBUILD"])

if test -n "$PDFLATEX" -a -n "$BIBTEX"; then
  AX_LATEX_CLASS([beamer], [ax_latex_have_beamer])
fi
AM_CONDITIONAL([HAVE_BEAMER], [test "$ax_latex_have_beamer" = yes])

AC_PROG_F77
AC_F77_LIBRARY_LDFLAGS
case ${host_os} in
  *darwin*)
    # * OS X doesn't need to link against the Fortran libraries because
    #   everything is contained within the vecLib framework.
    FLIBS=''
    ;;
esac

if test -n "$F77"; then
   AX_BLAS
   AX_LAPACK
fi
AM_CONDITIONAL([HAVE_BLAS], [test "$ax_blas_ok" = yes])
AM_CONDITIONAL([HAVE_LAPACK], [test "$ax_lapack_ok" = yes])

AC_CONFIG_FILES([Makefile
                 VERSION
                 doc/Makefile
                 doc/manual/Makefile
                 doc/manual/utils/version.py
                 doc/parallel/Makefile
                 doc/internals/Makefile
                 doc/gsa/Makefile
                 doc/dseries-and-reporting/Makefile
                 tests/Makefile
                 matlab/dynare_version.m
                 windows/dynare-version.nsi
                 dynare++/Makefile
                 dynare++/parser/cc/Makefile
                 dynare++/sylv/Makefile
                 dynare++/sylv/cc/Makefile
                 dynare++/sylv/testing/Makefile
                 dynare++/tl/Makefile
                 dynare++/tl/cc/Makefile
                 dynare++/tl/testing/Makefile
                 dynare++/doc/Makefile
                 dynare++/utils/cc/Makefile
                 dynare++/integ/Makefile
                 dynare++/integ/cc/Makefile
                 dynare++/integ/src/Makefile
                 dynare++/integ/testing/Makefile
                 dynare++/kord/Makefile
                 dynare++/src/Makefile
                 mex/sources/Makefile
])

AC_ARG_ENABLE([matlab], AS_HELP_STRING([--disable-matlab], [disable compilation of MEX files for MATLAB]), [], [enable_matlab=yes])
if test "$enable_matlab" = yes; then
  AC_CONFIG_SUBDIRS([mex/build/matlab])
  AX_MATLAB
  AX_MATLAB_BATCH_OPTIONS
fi
AM_CONDITIONAL([ENABLE_MATLAB], [test "$enable_matlab" = yes])
AM_CONDITIONAL([HAVE_MATLAB], [test "$ax_enable_matlab" = yes])

AC_ARG_ENABLE([octave], AS_HELP_STRING([--disable-octave], [disable compilation of MEX files for Octave]), [], [enable_octave=yes])
if test "$enable_octave" = yes; then
  AC_CONFIG_SUBDIRS([mex/build/octave])
  AC_CHECK_PROG([OCTAVE], [octave], [octave])
fi
AM_CONDITIONAL([ENABLE_OCTAVE], [test "$enable_octave" = yes])
AM_CONDITIONAL([HAVE_OCTAVE], [test "$enable_octave" = yes -a -n "$OCTAVE"])

# Enable exporting of Org files
# The clean way would be to test for Emacs, Org-mode, latex, dvipng...
AC_ARG_ENABLE([org-export], AS_HELP_STRING([--enable-org-export], [enable exporting of Org files (requires Emacs, org-mode and other external programs)]))
AM_CONDITIONAL([ENABLE_ORG_EXPORT], [test -n "$enable_org_export"])

# Construct final output message

if test "$ax_blas_ok" = yes -a "$ax_lapack_ok" = yes -a "$has_matio" = yes; then
  BUILD_DYNAREPLUSPLUS="yes"
else
  BUILD_DYNAREPLUSPLUS="no (missing one of: BLAS, LAPACK, MatIO)"
fi

if test -n "$PDFLATEX" -a "$ax_latex_have_beamer" = yes; then
  BUILD_BEAMER_DOC="yes"
else
  BUILD_BEAMER_DOC="no (missing one of: pdflatex, beamer)"
fi

if test "x$PDFLATEX" != "x"; then
  BUILD_OTHER_PDF_DOC="yes"
else
  BUILD_OTHER_PDF_DOC="no (missing pdflatex)"
fi

if test -n "$enable_org_export"; then
  BUILD_DYNARE_INTERNAL_DOC="yes"
else
  BUILD_DYNARE_INTERNAL_DOC="no (Org export not enabled)"
fi

if test -n "$SPHINXBUILD"; then
  BUILD_DYNARE_HTML_MANUAL="yes"
  BUILD_DYNARE_PDF_MANUAL="yes"
else
  BUILD_DYNARE_HTML_MANUAL="no (missing sphinx-build)"
  BUILD_DYNARE_PDF_MANUAL="no (missing sphinx-build)"
fi

if test -n "$OCTAVE"; then
  TESTSUITE_OCTAVE="yes"
else
  TESTSUITE_OCTAVE="no"
fi

if test "$ax_blas_ok" = yes -a "$ax_lapack_ok" = yes; then
  TESTSUITE_DYNAREPLUSPLUS="yes"
else
  TESTSUITE_DYNAREPLUSPLUS="no"
fi

AC_MSG_NOTICE([

Dynare is now configured for building the following components...

Binaries (with "make"):
 Dynare++:                              $BUILD_DYNAREPLUSPLUS

PDF documentation (with "make pdf"):
 Dynare reference manual:               $BUILD_DYNARE_PDF_MANUAL
 Beamer presentations:                  $BUILD_BEAMER_DOC
 Various other documents:               $BUILD_OTHER_PDF_DOC

HTML documentation (with "make html"):
 Dynare reference manual:               $BUILD_DYNARE_HTML_MANUAL
 Dynare internal doc:                   $BUILD_DYNARE_INTERNAL_DOC

Testsuites (run with "make check"):
 Dynare for Octave:                     $TESTSUITE_OCTAVE
 Dynare++:                              $TESTSUITE_DYNAREPLUSPLUS
])

AC_OUTPUT
