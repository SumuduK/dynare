The Dynare Reference Manual, version 4.6-unstable.
==================================================

Currently the development team of Dynare is composed of:

* Stéphane Adjemian
* Houtan Bastani
* Michel Juillard
* Frédéric Karamé
* Junior Maih
* Ferhat Mihoubi
* Willi Mutschler
* Johannes Pfeifer
* Marco Ratto
* Sébastien Villemot

Copyright © 1996-2019, Dynare Team.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

A copy of the license can be found at `http://www.gnu.org/licenses/fdl.txt <http://www.gnu.org/licenses/fdl.txt>`_. 

Table of contents:

.. toctree::
   :numbered:
   :maxdepth: 4
   
   introduction
   installation-and-configuration
   running-dynare
   the-model-file
   the-configuration-file
   time-series
   reporting
   examples
   dynare-misc-commands
   bibliography

.. only:: builder_html

   Indices and tables
   ==================

	* :ref:`genindex`
